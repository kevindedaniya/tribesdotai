# tribesdotai

### [Vercel Link (https://tribesdotai.vercel.app)](https://tribesdotai.vercel.app/)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
